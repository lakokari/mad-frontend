import React, { Component } from 'react';
import { extend } from "lodash";

import './App.css';

import { SearchkitManager,SearchkitProvider,
  SearchBox, RefinementListFilter, Pagination,
  HitsStats, SortingSelector,
  ResetFilters, 
  ViewSwitcherHits, ViewSwitcherToggle, 
  GroupedSelectedFilters,
  Layout, TopBar, LayoutBody, LayoutResults,
  ActionBar, ActionBarRow, SideBar } from 'searchkit'


const sk = new SearchkitManager("http://188.166.224.27:9200/bpcbjateng/", {
  basicAuth:"elastic:changeme"
})


const MovieHitsGridItem = (props)=> {
  const {bemBlocks, result} = props
  let url = "http://www.imdb.com/title/" + result._source.imdbId
  let imgurl = result._source.foto_1 ? 'https://bpcbjateng.id/'+result._source.foto_1 : 'http://placeimg.com/170/240/arch'
  const source = extend({}, result._source, result.highlight)
  return (
    <div className={bemBlocks.item().mix(bemBlocks.container("item"))} data-qa="hit">
      <a href={url} target="_blank" rel="noopener noreferrer">
        <img data-qa="poster" alt="presentation" className={bemBlocks.item("poster")} src={imgurl} width="170" height="240"/>
        <div data-qa="customer_full_name" className={bemBlocks.item("customer_full_name")} dangerouslySetInnerHTML={{__html: source.kode_benda + ' (' + source.nama_benda + ')'}}/>        
      </a>
    </div>
  )
}

const MovieHitsListItem = (props)=> {
  const {bemBlocks, result} = props
  let url = "http://www.imdb.com/title/" + result._source.imdbId
  let imgurl = result._source.foto_1 ? 'https://bpcbjateng.id/'+result._source.foto_1 : 'http://placeimg.com/170/240/arch'
  const source = extend({}, result._source, result.highlight)
  return (
    <div className={bemBlocks.item().mix(bemBlocks.container("item"))} data-qa="hit">
      <div className={bemBlocks.item("poster")}>
        <img alt="presentation" data-qa="poster" src={imgurl} />
      </div>
      <div className={bemBlocks.item("details")}>
        <a href={url} target="_blank" rel="noopener noreferrer"><h2 className={bemBlocks.item("title")} dangerouslySetInnerHTML={{__html:source.nama_benda}}></h2></a>
        <h3 className={bemBlocks.item("subtitle")}>Gender: {source.nama_benda}</h3>
        <div className={bemBlocks.item("text")} dangerouslySetInnerHTML={{__html:source.nama_benda}}></div>
      </div>
    </div>
  )
}

class App extends Component {
  render() {
    return (

    <SearchkitProvider searchkit={sk}>
      <Layout>
        <TopBar>
          <SearchBox
          searchOnChange={true}
          queryOptions={{analyzer:"standard"}}
          queryFields={["nama_benda^5"]}/>
          
                  
        </TopBar>
        <LayoutBody>
          <SideBar>            
          <RefinementListFilter multi={true} operator="OR" id="kondisi" title="Kondisi" field="kondisi.keyword" size={5} />
          <RefinementListFilter multi={true} operator="OR" id="periode" title="Periode" field="periode.keyword" size={5} />
          <RefinementListFilter multi={true} operator="OR" id="bahan" title="Bahan" field="bahan.keyword" size={5} />
          <RefinementListFilter multi={true} operator="OR" id="bentuk" title="Bentuk" field="bentuk.keyword" size={5} />
          <RefinementListFilter multi={true} operator="OR" id="status" title="Status" field="status.keyword" size={5} />
          
          
          </SideBar>
          <LayoutResults>
            <ActionBar>
            <ActionBarRow>
                <HitsStats translations={{
                  "hitstats.results_found":"{hitCount} hasil ditemukan"
                }}/>
                <ViewSwitcherToggle/>
                <SortingSelector options={[
                  {label:"Relevansi", field:"_score", order:"desc"},
                  
                ]}/>
            </ActionBarRow>
            <ActionBarRow>
                <GroupedSelectedFilters/>
                <ResetFilters/>
            </ActionBarRow>
            </ActionBar>
            <ViewSwitcherHits
                hitsPerPage={15} highlightFields={["nama_benda", "kode_benda", "bahan"]}
                sourceFilter={["nama_benda", "foto_1", "foto_2", "foto_3", "foto_4", "bahan", "kode_benda"]}
                hitComponents={[
                  {key:"grid", title:"Grid", itemComponent:MovieHitsGridItem, defaultOption:true},
                  {key:"list", title:"List", itemComponent:MovieHitsListItem}
                ]}
                scrollTo="body"
            />
          
          <Pagination showNumbers={true}/>
          </LayoutResults>
        </LayoutBody>
      </Layout>
        
         
  </SearchkitProvider>

);
}
}

export default App;
